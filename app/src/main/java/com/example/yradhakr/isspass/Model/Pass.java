package com.example.yradhakr.isspass.Model;

import com.google.gson.annotations.SerializedName;

public class Pass {

    @SerializedName("duration")
    long duration;

    @SerializedName("risetime")
    long risetime;

    public long getDuration() { return duration; }

    public long getRisetime() { return risetime; }
}
