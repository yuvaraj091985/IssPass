package com.example.yradhakr.isspass.Contract;

import com.example.yradhakr.isspass.Model.Pass;

import java.util.List;

public class IssPassContract {

    public interface View {
        void updateList(List<Pass> passes);
    }

    public interface Presenter {
        void getPassResponse(Double lat, Double lon);
    }

}
