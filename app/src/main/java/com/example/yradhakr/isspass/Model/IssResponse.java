package com.example.yradhakr.isspass.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IssResponse {

    @SerializedName("message")
    String mMessage;

    @SerializedName("response")
    List<Pass> mPasses;

    public String getMessage() { return mMessage; }

    public List<Pass> getPasses() { return mPasses; }

}
