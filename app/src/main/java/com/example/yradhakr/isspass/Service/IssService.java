package com.example.yradhakr.isspass.Service;

import com.example.yradhakr.isspass.Model.IssResponse;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

public interface IssService {

    @Headers("contentType: application/json")
    @GET("iss-pass.json")
    Observable<IssResponse> getIssResponse(@Query("lat") double lat, @Query("lon") double lng);
}
