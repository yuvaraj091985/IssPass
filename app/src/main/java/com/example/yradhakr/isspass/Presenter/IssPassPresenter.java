package com.example.yradhakr.isspass.Presenter;

import com.example.yradhakr.isspass.Contract.IssPassContract;
import com.example.yradhakr.isspass.Model.IssResponse;
import com.example.yradhakr.isspass.NetworkClient.RetroFitClient;
import com.example.yradhakr.isspass.Service.IssService;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class IssPassPresenter implements IssPassContract.Presenter {

    private IssPassContract.View mActivity;

    public IssPassPresenter(IssPassContract.View view) {
        mActivity = view;
    }

    @Override
    public void getPassResponse(Double lat, Double lon) {
            IssService service = RetroFitClient.getRetroFitInstance("http://api.open-notify.org/").create(IssService.class);

            service.getIssResponse(lat, lon).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<IssResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(IssResponse issResponse) {
                            if(issResponse != null) {
                                mActivity.updateList(issResponse.getPasses());
                            }
                        }
                    });
        }

}
