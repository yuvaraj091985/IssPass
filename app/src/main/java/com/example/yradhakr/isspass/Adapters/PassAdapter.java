package com.example.yradhakr.isspass.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yradhakr.isspass.Model.Pass;
import com.example.yradhakr.isspass.R;
import com.example.yradhakr.isspass.Utils.DateUtil;

import java.util.Calendar;
import java.util.List;

public class PassAdapter extends RecyclerView.Adapter<PassAdapter.PassItemViewHolder> {

    public List<Pass> mPassList;

    public PassAdapter(List<Pass> passList) {
        mPassList = passList;
    }

    @Override
    public PassItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pass_line_item, parent, false);

        return new PassItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PassItemViewHolder holder, int position) {
        Pass pass = mPassList.get(position);

        Calendar formattedCalendar = DateUtil.convertLongToDate(pass.getRisetime());

        holder.duration.setText("Duration :" + String.valueOf(pass.getDuration() +" Year :" +formattedCalendar.get(Calendar.YEAR) +
        " Month :" + (formattedCalendar.get(Calendar.MONTH) + 1) + " Day:" + formattedCalendar.get(Calendar.WEEK_OF_MONTH) +
        " Hour:" + formattedCalendar.get(Calendar.HOUR_OF_DAY) + " Minute:" + formattedCalendar.get(Calendar.MINUTE)));
    }

    @Override
    public int getItemCount() {
        return mPassList.size();
    }

    public void updateList(List<Pass> passList) {
        mPassList = passList;
        notifyDataSetChanged();
    }

    public class PassItemViewHolder extends RecyclerView.ViewHolder {

        TextView duration;

        public PassItemViewHolder(View itemView) {
            super(itemView);

            duration = (TextView) itemView.findViewById(R.id.duration);
        }
    }
}
